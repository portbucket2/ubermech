﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionReporter : MonoBehaviour
{
    public bool logAll;
    private void OnCollisionEnter(Collision collision)
    {
        BaseMachineBehaviour bmb = collision.collider.GetComponentInParent<BaseMachineBehaviour>();
        if (bmb || logAll)
        {
            Debug.Log( GetGameObjectPath( collision.collider.transform));
        }
    }
    private static string GetGameObjectPath(Transform transform)
    {
        string path = transform.name;
        while (transform.parent != null)
        {
            transform = transform.parent;
            path = transform.name + "/" + path;
        }
        return path;
    }
}

