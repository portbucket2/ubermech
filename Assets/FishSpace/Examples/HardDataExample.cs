﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using FishSpace;

public class HardDataExample : MonoBehaviour 
{
    
    public bool soundsState;
    public string playerName;
    public int highScore;
    public float bestTime;
 
    public Text datatext;

    public void ShowValues()
    {
        datatext.text = "High scorer: " + SampleUserSettings.HighScorerName.value.ToString() + "\n" +
            "High score: " + SampleUserSettings.HighScore.value.ToString() + "\n" +
            "Best time: " + SampleUserSettings.BestTime.value.ToString() + "\n" +
            "Sounds enabled: " + SampleUserSettings.SoundsOn.value.ToString();
//        print(SampleUserSettings.HighScorerName.value);
//        print(SampleUserSettings.HighScore.value);
//        print(SampleUserSettings.BestTime.value);
//        print(SampleUserSettings.SoundsOn.value);
    }
    public void SetMyValues()
    {
        SampleUserSettings.SoundsOn.value = soundsState;
        SampleUserSettings.HighScore.value = highScore;
        SampleUserSettings.HighScorerName.value = playerName;
        SampleUserSettings.BestTime.value = bestTime;
    }

    public void ResetAllDataToDefault()
    {
        HardDataCleaner.Clean();
    }

}
public static class SampleUserSettings
{
    public static HardData<bool> SoundsOn = new HardData<bool>("SoundsKey",true);
    public static HardData<int> HighScore = new HardData<int>("HighScoreKey", 0);
    public static HardData<float> BestTime = new HardData<float>("BestTimeKey", -1.0f);
    public static HardData<string> HighScorerName = new HardData<string>("HighScorerNameKey","DefaultPlayer");
}