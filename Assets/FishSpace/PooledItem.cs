﻿using UnityEngine;
using System.Collections.Generic;
namespace FishSpace
{
    public class PooledItem : MonoBehaviour
    {
        public bool alive  = true;
        public int useCount = 0;
        public GameObject original;

        void OnDestroy() { if (alive) FishSpace.Pool.Destroy(gameObject); }

        void Awake()
        {
            if (!alive && original != null)
            {
                Pool.RegisterToPool(this);
            }
        }

        //bool initialized = false;

        //public void Init()
        //{
        //    if (initialized) return;


        //}
    }

}
