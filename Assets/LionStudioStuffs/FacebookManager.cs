﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init();
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }


    }
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
       
        
    }

    public static void LogLevelCompleted(int levelnumber=0)
    {
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = ""+ levelnumber;

        FB.LogAppEvent(AppEventName.AchievedLevel, null, Params);
        Debug.Log("loglevelcompleted");

    }

    public static void LogLevelStarted(int levelnumber=0)
    {
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;

        FB.LogAppEvent("Level Started", null, Params);

    }

    public static void LogLevelReStarted(int levelnumber = 0)
    {
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;

        FB.LogAppEvent("Level ReStarted", null, Params);

    }

    public static void LogLevelFailed(int levelnumber = 0)
    {
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;

        FB.LogAppEvent("Level Failed", null, Params);

    }

    public static void LogNotificationAccepted(string status)
    {
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = status;


        FB.LogAppEvent("Notification Accepted", null, Params);

    }



    // Update is called once per frame
    void Update()
    {
        
    }


}
