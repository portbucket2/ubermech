﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoLeverTriggerController : MonoBehaviour
{
    public List<Collider> colliderList = new List<Collider>();


    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log("in");
        colliderList.Add(other);
    }
    public void OnTriggerExit(Collider other)
    {
        //Debug.Log("in");
        colliderList.Remove(other);
    }
}
