﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCompleteFunctionality : MonoBehaviour
{
    public LevelLoader levelLoader { get { return LevelLoader.instance; } }
    public AreaLoader areaLoader;
    public GameObject wantsPanel;

    FishSpace.HardData<int> wantsMoreLevels;
    public int areaIndex;
    private void Start()
    {

        wantsMoreLevels = new FishSpace.HardData<int>("WANTS_MORE_LEVEL",0);
        if (wantsMoreLevels.value != 0)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
           Decide();
           areaLoader.onRefresh += Decide;
        }


    }

    public void Decide()
    {
        
        LevelArea area = levelLoader.levelAreas[areaIndex];
            bool willBeEnabled = area.lastUnlockedLevelIndex.value >= area.levelPrefs.Count;
            this.gameObject.SetActive(willBeEnabled);


    }

    public void OnMainButtonClick()
    {
        wantsPanel.SetActive(true);
    }

    public void OnNotificationResponse(bool accepted)
    {
        wantsPanel.SetActive(false);
        this.gameObject.SetActive(false);
        wantsMoreLevels.value = accepted ?1:-1;
        FacebookManager.LogNotificationAccepted(accepted.ToString());
    }
}
