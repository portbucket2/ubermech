﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;
using System;

public class LevelLoader : MonoBehaviour
{
    public List<LevelArea> levelAreas;

    //public HardData<int> starCount;
    //public HardData<int> areaIndex;
    //public HardData<int> currentLevelIndex;

    public static LevelLoader instance;

    public bool allLevelsUnlocked=false;

    public void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.transform.root.gameObject);

            //starCount = new HardData<int>("STAR", 0);
            //areaIndex = new HardData<int>("AREA", 0);
            //currentLevelIndex = new HardData<int>("LEVEL", 0);


            for (int a = 0; a < levelAreas.Count; a++)
            {
                LevelArea area = levelAreas[a];
                area.lastUnlockedLevelIndex = new HardData<int>(string.Format("UNLOCKED_PROGRSS_{0}", a), 0);
                area.stars = new List<HardData<int>>();
                for (int l = 0; l < area.levelPrefs.Count; l++)
                {
                    area.stars.Add(new HardData<int>(string.Format("stars_{0}_{1}", a, l), 0));
                }
            }
        }


    }

    public int GetStarFor(int areaIndex, int levelIndex)
    {
        return levelAreas[areaIndex].stars[levelIndex].value;
    }
    public void SetStarFor(int areaIndex, int levelIndex, int star)
    {
        levelAreas[areaIndex].stars[levelIndex].value = star;
    }
    public int GetTotalStarCount()
    {

        int count = 0;
        foreach (LevelArea area in levelAreas)
        {
            foreach (HardData<int> star in area.stars)
            {
                count += star.value;
            }
        }
        return count;
    }
    public bool IsAreaUnlocked(int areaIndex)
    {
        if (levelAreas[areaIndex].starToEnter > GetTotalStarCount()) return false;
        if (areaIndex == 0) return true;
        else if (levelAreas[areaIndex - 1].lastUnlockedLevelIndex.value >= (levelAreas[areaIndex - 1].levelPrefs.Count ))
        {
            return true;
        }
        else return false;
    }
    public int GetLastUnlockedAreaIndex()
    {
        for (int i = 0; i < levelAreas.Count; i++)
        {
            if (!IsAreaUnlocked(i)) return i - 1;
        }
        if (levelAreas.Count == 0) return -1;
        else return levelAreas.Count - 1;
    }

    public int GetLevelNumber(int areaIndex, int levelIndex)
    {
        if (areaIndex >= levelAreas.Count) return -1;
        if (levelIndex >= levelAreas[areaIndex].levelPrefs.Count) return -1;

        int number = 1;
        for (int i = 0; i < areaIndex; i++)
        {
            number += levelAreas[i].levelPrefs.Count;
        }
        return number + levelIndex;
    }

    //public bool IsLevelLocked(int areaIndex, int levelIndex)
    //{
    //    if (IsAreaLocked(areaIndex)) return true;
    //    else
    //    {
    //        return levelAreas[areaIndex].lastUnlockedLevelIndex.value < levelIndex;
    //    }
    //}

    public static string GetLevelName(int areaI, int levelI)
    {
        return string.Format("Level {0}-{1}",areaI+1,levelI+1);
    }
}
[System.Serializable]
public class LevelArea
{
    public int starToEnter=0;
    public List<LevelPrefabManager> levelPrefs;
    [NonSerialized]
    public List<HardData<int>> stars;
    [NonSerialized]
    public HardData<int> lastUnlockedLevelIndex;
} 