﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    public static MainGameManager instance;
    public InLevelCanvasManager levelCanvas;
    public GameObject loaderObjects;

    public AutoAreaLoader areaLoader;
    // Start is called before the first frame update
    void Awake()
    {
        Application.targetFrameRate = 300;
        instance = this;
        levelCanvas.gameObject.SetActive(false);
    }

    public LevelPrefabManager runningLevelManager
    {
        get
        {
            return LevelPrefabManager.currentLevel;
        }
    }


    public void StartLevel(int areaIndex,int levelIndex )
    {
        if (runningLevelManager)
        {
            EndLevel();
        }
        levelCanvas.gameObject.SetActive(true);
        GameObject prefab = LevelLoader.instance.levelAreas[areaIndex].levelPrefs[levelIndex].gameObject;
        Instantiate(prefab, Vector3.zero, Quaternion.identity).GetComponent<LevelPrefabManager>().Load(areaIndex, levelIndex, EndLevel);
        loaderObjects.SetActive(false);
    }
    public void EndLevel()
    {
        levelCanvas.gameObject.SetActive(false);
        loaderObjects.SetActive(true);
        areaLoader.RestartLoader();
        
    }


    public static void ReportLevelCompletion(bool success)
    {
        if (instance)
            instance.runningLevelManager.OnComplete(success);
        else
            Debug.LogError("No level instance running");
    }
}
