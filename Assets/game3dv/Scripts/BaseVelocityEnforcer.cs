﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseVelocityEnforcer : MonoBehaviour,MotionModifier
{
    public bool log;
    public float velMag = 3;
    public float lerpRate = 1;

    public List<Transform> endPoints;

    float MotionModifier.GetTargetSpeed(float current)
    {
        return velMag;
    }
    float MotionModifier.GetLerpRate()
    {
        return lerpRate;
    }
    Vector3 MotionModifier.GetAcc()
    {
        return Vector3.zero;
    }

    void MotionModifier.ExitBehaviour(EnforcableMachine em)
    {
        if (!MachineResetManager.instance.isPlaying) return;

        float shortestDist = float.MaxValue;
        Transform closest = null;

        foreach (var item in endPoints)
        {
            if (closest == null)
            {
                closest = item;
                shortestDist = Vector3.Distance(item.position, em.transform.position);
            }
            else
            {
                float dist = Vector3.Distance(item.position, em.transform.position);
                if (dist < shortestDist)
                {
                    closest = item;
                    shortestDist = dist;
                }
            }
        }

        if (closest)
        {


            Vector3 transPos = closest.InverseTransformPoint(em.transform.position);

            transPos.x = 0;

            em.transform.position = closest.TransformPoint(transPos);


            em.rgbd.velocity = closest.forward * velMag;

            //em.rgbd.SetHorizontalSpinByVelocity(2);
            if(log)Debug.LogFormat("Exit-pos{1},vel:{0}", em.rgbd.velocity, em.transform.position);
        }
    }
}
