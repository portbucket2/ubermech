﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeRotationTutorial : MonoBehaviour
{
    public GameObject stage1;
   //ublic GameObject stage2;
    public Text tutText;
    public string firstInstruction = "Tap to select";
    //blic string secondInstruction = "tap to place";


    bool unsub2;
    private void Start()
    {
        FindObjectOfType<RotationManager>().enabled = false;
        MachineDropManager.instance.onNewMachineBeingPicked += MachineTapped;
        stage1.SetActive(false);
        tutText.gameObject.SetActive(false);

    }

    private void MachineTapped(MechType type)
    {
        stage1.SetActive(true);
        tutText.text = firstInstruction;
        tutText.gameObject.SetActive(true);
        MachineDropManager.instance.onNewMachineBeingPicked -= MachineTapped;
        InputManager.instance.onSwipe += OnSwipe;

        FishSpace.Centralizer.Add_DelayedMonoAct(this,() =>
        FindObjectOfType<RotationManager>().enabled = true,0.2f,true);
    }

    public Dir requiredSipeDir;
    private void OnSwipe(Dir dir)
    {
        if (dir == requiredSipeDir)
        {
            InputManager.instance.onSwipe -= OnSwipe;
            stage1.SetActive(false);
            tutText.gameObject.SetActive(false);
            this.gameObject.SetActive(true);
        }

    }
}

