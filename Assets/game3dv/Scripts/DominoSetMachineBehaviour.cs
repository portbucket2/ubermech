﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoSetMachineBehaviour : BaseMachineBehaviour
{
    public List<Rigidbody> dominos;

    public List<MachineTransformState> transStates = new List<MachineTransformState>();

    private void Awake()
    {

    }

    public override void OnInit()
    {
        base.OnInit();

        foreach (Rigidbody rgbd in dominos)
        {
            MachineTransformState mts = new MachineTransformState(rgbd.transform, false, "");
            transStates.Add(mts);
        }
    }
    public override void OnReset()
    {
        base.OnReset();
        for (int i = 0; i < dominos.Count; i++)
        {
            Rigidbody rgbd = dominos[i];
            rgbd.isKinematic = true;
            transStates[i].ApplyState(rgbd.transform);
        }
    }
    public override void OnPlay()
    {
        base.OnPlay();
        for (int i = 0; i < dominos.Count; i++)
        {
            Rigidbody rgbd = dominos[i];
            rgbd.isKinematic = false;
        }
    }
    public override void OnStop()
    {
        base.OnStop();
        for (int i = 0; i < dominos.Count; i++)
        {
            Rigidbody rgbd = dominos[i];
            rgbd.isKinematic = true;
            transStates[i].ApplyState(rgbd.transform);
        }
    }
}
