﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropButtonController : MonoBehaviour//, IPointerDownHandler
{ 
    public Text countText;
    public Button clickButt;
    public Transform sampleSpwnPoint;
    public Image targetImage;
    //public PointerPressedDetect ppd;
    int count;
    MechType type;

    BaseMachineBehaviour sampleMech;


    public void Load(BaseMachineBehaviour bmb, int count, System.Action<MechType> onClick)
    {
        sampleMech = Instantiate(bmb.gameObject, sampleSpwnPoint).GetComponent<BaseMachineBehaviour>();

        sampleMech.transform.localPosition = bmb.uiPlacementTransform.localPosition;
        sampleMech.transform.localRotation = bmb.uiPlacementTransform.localRotation;
        sampleMech.transform.localScale = bmb.uiPlacementTransform.localScale;

        //sampleMech.isPlacable = false;
        sampleMech.isDisplay = true;
        type = sampleMech.type;
        ChangeCount(count);

        clickButt.onClick.RemoveAllListeners();
        clickButt.onClick.AddListener(() =>
        {
            //boost = true;
            //FishSpace.Centralizer.Add_DelayedMonoAct(this,()=> { boost = false; },0.1f,true);
            onClick?.Invoke(type);
        }
        );
        //localPosOnLoad = clickButt.transform.localPosition;

    }
    public void ChangeCount(int newCount)
    {
        count = newCount;
        countText.text = string.Format("x{0}", count);
    }
    private void Start()
    {
        MachineDropManager.instance.onNewMachineBeingPicked += OnNewMachineEvent;
        baseColor = targetImage.color;
        isHighlighted = false;
    }


    public GameObject highLightObject;
    bool _isHighlighted;
    Color baseColor;
    bool isHighlighted
    {
        get { return _isHighlighted;  }
        set { _isHighlighted = value;

            highLightObject.SetActive(value);
            targetImage.color = baseColor * (value ? 0.83f * 2 : 1);
        }
    }
    void OnNewMachineEvent(MechType chosenType)
    {
        isHighlighted = chosenType == type;
    }


}
