﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnforcableMachine : MonoBehaviour
{
    public Rigidbody rgbd;
    public float personalTrampolineBonusPower = 0;

    public List<MotionModifier> enforcers = new List<MotionModifier>();
    public MotionModifier lastEnforcer
    {
        get
        {
            if (enforcers.Count == 0) return null;
            else return enforcers[enforcers.Count - 1];
        }
    }

    private void FixedUpdate()
    {
        if (lastEnforcer != null)
        {

            float currentMag = rgbd.velocity.magnitude;

            Vector3 suggestedVel = rgbd.velocity.normalized * Mathf.Lerp(currentMag, lastEnforcer.GetTargetSpeed(currentMag), lastEnforcer.GetLerpRate() * Time.fixedDeltaTime);
            rgbd.AddForce(suggestedVel - rgbd.velocity, ForceMode.VelocityChange);

            Vector3 acc = lastEnforcer.GetAcc();
            if (acc.sqrMagnitude > 0)
            {
                rgbd.AddForce(lastEnforcer.GetAcc(), ForceMode.Acceleration);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        MotionModifier ve = other.GetComponentInParent<MotionModifier>();

        if (ve != null)
        {
            enforcers.Add(ve);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        MotionModifier ve = other.GetComponentInParent<MotionModifier>();

        if (ve != null)
        {
            
            enforcers.Remove(ve);

            ve.ExitBehaviour(this);
            //Debug.LogFormat("{2}-vel:{0},mag{1}",rgbd.velocity,rgbd.velocity.magnitude,(ve as MonoBehaviour).transform.parent.name);
        }
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    VelocityEnforcer ve = other.GetComponentInParent<VelocityEnforcer>();

    //    if (ve!=null)
    //    {
    //        float currentMag =  rgbd.velocity.magnitude;

    //        Vector3 suggestedVel = rgbd.velocity.normalized * Mathf.Lerp(currentMag,ve.GetMag(),ve.GetLerpRate());


    //        rgbd.AddForce(suggestedVel - rgbd.velocity, ForceMode.VelocityChange);
    //    }
    //}
}
public interface MotionModifier
{
    float GetTargetSpeed(float current);
    float GetLerpRate();
    Vector3 GetAcc();
    void ExitBehaviour(EnforcableMachine em);
}