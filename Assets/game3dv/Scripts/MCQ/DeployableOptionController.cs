﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BaseMachineBehaviour))]
public class DeployableOptionController : MonoBehaviour
{
    public BaseMachineBehaviour _mech;
    public BaseMachineBehaviour mech
    {
        get
        {
            if (!_mech)
            {
                _mech = GetComponent<BaseMachineBehaviour>();
            }
            return _mech;
        }
    }
    public MechType type
    {
        get
        {
            return mech.type;
        }
    }
    public List<MechType> optionTypes;
}
