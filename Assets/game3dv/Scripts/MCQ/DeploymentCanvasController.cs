﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FishSpace;

public class DeploymentCanvasController : MonoBehaviour
{


     List<DeploymentChoiceButtonController> buttonList = new List<DeploymentChoiceButtonController>();

    public  RectTransform machineDropPoint;

    [SerializeField] MechPrefabListUp prefabKeep;
    [SerializeField] GameObject uiButtonPrefab;
    



    public void LoadWith(DeployableOptionController doc)
    {
        //return;
        ClearList();
        foreach (var item in doc.optionTypes)
        {
            GameObject uiButtonObj = Instantiate(uiButtonPrefab, machineDropPoint);
            DeploymentChoiceButtonController dropButtonRef = uiButtonObj.GetComponent<DeploymentChoiceButtonController>();
            dropButtonRef.Load(prefabKeep.prefabs[item], OnItemClick);
            buttonList.Add(dropButtonRef);
        }
    }
    void ClearList()
    {
        for (int i = buttonList.Count-1; i >=0; i--)
        {
            Destroy(buttonList[i].gameObject);
            buttonList.RemoveAt(i);
        }
    }

    public bool OnItemClick(MechType mechType)
    {
       return  MachineDeploymentManager.instance.SubmitPlayerChoice(mechType);
    }
}
