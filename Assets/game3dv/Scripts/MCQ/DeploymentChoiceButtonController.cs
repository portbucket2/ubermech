﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeploymentChoiceButtonController : MonoBehaviour
{
    public Button clickButt;
    public Transform sampleSpwnPoint;
    public Image targetImage;

    public GameObject outlineObject;
    public Image outlineImage;
    public Color colorPositive;
    public Color colorNegative;

    MechType type;
    public BaseMachineBehaviour sampleMech;

    public void Load(GameObject samplePrefab, System.Func<MechType,bool> onChoiceMade)
    {

        sampleMech = Instantiate(samplePrefab, sampleSpwnPoint).GetComponent<BaseMachineBehaviour>();


        sampleMech.transform.localPosition = sampleMech.uiPlacementTransform.localPosition;
        sampleMech.transform.localRotation = sampleMech.uiPlacementTransform.localRotation;
        sampleMech.transform.localScale = sampleMech.uiPlacementTransform.localScale;

        sampleMech.isDisplay = true;
        type = sampleMech.type;
        clickButt.onClick.RemoveAllListeners();
        clickButt.onClick.AddListener(() =>
        {
            bool success = false;
            if(onChoiceMade!=null) success = onChoiceMade(type);
            clickButt.interactable = false;
            outlineImage.color = success ? colorPositive : colorNegative;
        }
        );
    }
}
