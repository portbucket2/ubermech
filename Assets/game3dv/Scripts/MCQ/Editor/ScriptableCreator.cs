﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class ScriptableCreator
{
        [UnityEditor.MenuItem("Assets/Create/MechPrefabList")]
        public static void Create()
        {
            MechPrefabListUp so = ScriptableObject.CreateInstance<MechPrefabListUp>();
            UnityEditor.AssetDatabase.CreateAsset(so, "Assets/MechList.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.EditorUtility.FocusProjectWindow();
            UnityEditor.Selection.activeObject = so;
        }
    }