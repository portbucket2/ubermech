﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartManager : MonoBehaviour
{
    public static HeartManager heartMan;
    public event Action<int> heartCountChanged;


    [SerializeField] private int heartCount = 3;



    public List<HeartVisual> heartlist = new List<HeartVisual>();

    public void Awake()
    {
        
        heartMan = this;
    }


    public Color imageColor_initial;
    public Color outlineColor_initial;
    public Color imageColor_Broken;
    public Color outlineColor_Broken;
    public Color imageColor_Success;
    public Color outlineColor_Success;

    private void Update()
    {
        for (int i = 0; i < heartlist.Count; i++)
        {
            if (i >= heartCount)
            {
                heartlist[i].Update(imageColor_Broken, outlineColor_Broken);
            }
            else if (success)
            {
                heartlist[i].Update(imageColor_Success, outlineColor_Success);
            }
            else
            {
                heartlist[i].Update(imageColor_initial, outlineColor_initial);
            }
        }
    }

    bool success = false;
    public void ReportSuccsessToHeartManager()
    {
        success = true;
    }
    public void ConsumeLife()
    {
        if (heartCount > 0)
        {
            heartCount--;
            heartCountChanged?.Invoke(heartCount);
            if (heartCount == 0)
            {
                MainGameManager.ReportLevelCompletion(false);
            }
        }
    }

}
[System.Serializable]
public class HeartVisual
{
    public Image image;
    public UnityEngine.UI.Outline outline;

    public void Update(Color imageColorTarget, Color outlineColorTarget)
    {
        image.color = Color.Lerp(image.color, imageColorTarget, 5*Time.deltaTime);
        outline.effectColor = Color.Lerp(outline.effectColor, outlineColorTarget, 5 * Time.deltaTime);
    }
}