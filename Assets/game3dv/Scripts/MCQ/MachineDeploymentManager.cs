﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FishSpace;

public class MachineDeploymentManager : MonoBehaviour
{


    public static MachineDeploymentManager instance;


    [SerializeField] private Transform machineRoot;
    [SerializeField] private DeploymentCanvasController deploymentCanvasCon;
    [SerializeField] MachineResetManager resetMan;
    [SerializeField] Button stopButton;


    private List<DeployableOptionController> docList = new List<DeployableOptionController>();
    int docIndex = -1;
    private DeployableOptionController CurrentDoc
    {
        get
        {
            if (docIndex >= 0 && docIndex < docList.Count)
            {
                return docList[docIndex];
            }
            else return null;
        }
    }

    void Awake()
    {
        instance = this;

        foreach (Transform tr in machineRoot)
        {
            DeployableOptionController doc = tr.GetComponent<DeployableOptionController>();
            if (doc)
            {
                docList.Add(doc);
            }
        }

        NextItem();

        resetMan.onPlay += onPlay;
        resetMan.onStop += onStop;

        stopButton.onClick.RemoveAllListeners();
        stopButton.onClick.AddListener(resetMan.StopAll);
    }
    void onPlay()
    {
        if (!resetMan.isDropComplete)
        {
            stopButton.gameObject.SetActive(true);
        }
        deploymentCanvasCon.machineDropPoint.gameObject.SetActive(false);
        //onNewMachineBeingPicked?.Invoke(MechType.NULL_MECH);
    }
    void onStop()
    {
        stopButton.gameObject.SetActive(false);
        if (!resetMan.isDropComplete)
        {
            deploymentCanvasCon.machineDropPoint.gameObject.SetActive(true);
        }
    }

    public void OnAllDone()
    {
        deploymentCanvasCon.machineDropPoint.gameObject.SetActive(false);
        resetMan.isDropComplete = true;
        PlayStopButtonController.FinalPlay(1.5f);
    }


    public void NextItem()
    {
        //Debug.Log(docList.Count);
        docIndex++;
        if (docIndex < docList.Count)
        {
            deploymentCanvasCon.LoadWith(docList[docIndex]);
            CurrentDoc.mech.HighlightAsCurrent();
        }
        else
        {
            HeartManager.heartMan.ReportSuccsessToHeartManager();
            OnAllDone();
        }
    }

    public bool SubmitPlayerChoice(MechType mt)
    {
        if (mt != CurrentDoc.type)
        {
            HeartManager.heartMan.ConsumeLife();
            return false;
        }
        else
        {
            CurrentDoc.mech.PlaceObject();
            NextItem();
            return true;
        }
    }

    static void DoVibrate(long timeInMilliseconds = 1000, int intensity = 255)
    {
        if (Application.platform != RuntimePlatform.Android) return;
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = unity.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass vibratorClass = new AndroidJavaClass("android.os.Vibrator");
        AndroidJavaObject vibratorService = ca.Call<AndroidJavaObject>("getSystemService", ca.GetStatic<AndroidJavaObject>("VIBRATOR_SERVICE"));
        AndroidJavaClass vibrationEffect = new AndroidJavaClass("android.os.VibrationEffect");
        AndroidJavaObject vibrationDetails = vibrationEffect.CallStatic<AndroidJavaObject>("createOneShot", timeInMilliseconds, intensity);

        vibratorService.Call("vibrate", vibrationDetails);
        unity.Dispose();
        ca.Dispose();
        vibratorClass.Dispose();
        vibratorService.Dispose();
        vibrationEffect.Dispose();
        vibrationDetails.Dispose();
    }
}
