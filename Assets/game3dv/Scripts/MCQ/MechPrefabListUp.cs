﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechPrefabListUp : ScriptableObject
{
    public List<BaseMachineBehaviour> listup;

    private Dictionary<MechType, GameObject> _prefabs;
    public Dictionary<MechType, GameObject> prefabs
    {
        get
        {
            if (_prefabs == null)
            {
                _prefabs = new Dictionary<MechType, GameObject>();
                foreach (var item in listup)
                {
                    _prefabs.Add(item.type,item.gameObject);
                }
            }
            return _prefabs;
        }
    }

}
