﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineResetManager : MonoBehaviour
{
    public static MachineResetManager instance;
    public event System.Action onStateChanged;
    public event System.Action onReset;
    public event System.Action onPlay;
    public event System.Action onStop;



    public static event System.Action<MachineResetManager> onMachineResetManagerNewInstance;

    //public CamRayObjectDrop camRayObjectDrop;

       
    private void Awake()
    {
        isDropComplete = false;
        isPlaying = false;
        instance = this;

        onMachineResetManagerNewInstance?.Invoke(this);
        //camRayObjectDrop.OnNewItemAddedByPlayer += PlayAll;
    }
    private void Start()
    {
        ResetAll();
    }


    internal bool isPlaying;
    private bool _isDropComplete;
    internal bool isDropComplete
    {
        get
        {
            return _isDropComplete;
        }
        set
        {
            if (value && !_isDropComplete)
            {
               if(MainGameManager.instance) MainGameManager.instance.runningLevelManager.ReportEarlyCompletion(1);
            }

            _isDropComplete = value;
        }

    }


    public void ResetAll()
    {
        StopAll();
        //Debug.Log(" onreset");
        onReset?.Invoke();
        onStateChanged?.Invoke();
    }

    public void PlayAll()
    {
        if (isPlaying) return;
        isPlaying = true;
        onPlay?.Invoke();
        onStateChanged?.Invoke();

    }

    public void StopAll()
    {
        if (isDropComplete || !isPlaying) return;
        isPlaying = false;
        onStop?.Invoke();
        onStateChanged?.Invoke();

    }
}
