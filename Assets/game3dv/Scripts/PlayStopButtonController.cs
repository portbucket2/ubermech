﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayStopButtonController : MonoBehaviour
{
    public Button playStopbutton;
    public Image playStopImage;
    public Sprite playSprite;
    public Sprite stopSprite;
    public Sprite completeSprite;

   public static PlayStopButtonController instance;
    private void Start()
    {
        instance = this;
        playStopbutton.onClick.RemoveAllListeners();
        playStopbutton.onClick.AddListener(OnClick);

        MachineResetManager.onMachineResetManagerNewInstance += OnNewResetManager;
        if (MachineResetManager.instance) OnNewResetManager(MachineResetManager.instance);
    }
    private void OnDestroy()
    {

        MachineResetManager.onMachineResetManagerNewInstance -= OnNewResetManager;
        if (registeredMachineResetMan)
        {
            registeredMachineResetMan.onStateChanged -= SetState;
        }
    }


    MachineResetManager registeredMachineResetMan;
    void OnNewResetManager(MachineResetManager instance)
    {
        if (instance != registeredMachineResetMan)
        {
            registeredMachineResetMan = instance;
            instance.onStateChanged += SetState;
            SetState();
        }
    }


    void SetState()
    {
        //playStopImage.sprite = registeredMachineResetMan.isDropComplete? completeSprite : ( registeredMachineResetMan.isPlaying ? stopSprite : playSprite);
        playStopImage.sprite = playSprite;
        if (registeredMachineResetMan.isDropComplete || registeredMachineResetMan.isPlaying)
        {
            playStopbutton.interactable = false;
        }
        else
        {
            playStopbutton.interactable = true;
        }
    }

    void OnClick()
    {
        if (registeredMachineResetMan)
        {
            if (registeredMachineResetMan.isDropComplete) return;
            else if (registeredMachineResetMan.isPlaying) registeredMachineResetMan.StopAll();
            else registeredMachineResetMan.PlayAll();
        }
    }

    public GameObject countDownTextRoot;
    public Text countDownText;
    IEnumerator CountDownPlay(float time)
    {
        float startTime = Time.time;
        float alottedTime = time;
        countDownTextRoot.SetActive(true);
        while (Time.time<startTime+alottedTime)
        {
            float progress = Mathf.Clamp(alottedTime - (Time.time - startTime), 0,alottedTime)  / alottedTime;
            int number = Mathf.CeilToInt(progress*2);
            string str;
            switch (number)
            {
                case 2:
                    str = "Ready?";
                    break;
                case 1:
                    str = "go...";
                    break;

                default:
                    str = "";
                    break;
            }

            countDownText.text = str;
            yield return null;
        }
        countDownTextRoot.SetActive(false);
        MachineResetManager.instance.PlayAll();
    }
    public static void FinalPlay(float counting = 0)
    {
        if (instance) instance.onFinalPlayCountDownStart?.Invoke();
        if (counting > 0 && instance)
        {
            //Debug.Log("routine called");
            instance.StartCoroutine(instance.CountDownPlay(counting));
        }
        else MachineResetManager.instance.PlayAll();
       
    }
    public event System.Action onFinalPlayCountDownStart;
    //private static void PlayStopToggle()
    //{
    //    if (MachineResetManager.instance)
    //    {
    //        if (MachineResetManager.instance.isDropComplete) return;
    //        else if (MachineResetManager.instance.isPlaying) MachineResetManager.instance.StopAll();
    //        else MachineResetManager.instance.PlayAll();
    //    }
    //}

    
}
