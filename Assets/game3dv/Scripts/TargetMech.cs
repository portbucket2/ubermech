﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMech : MonoBehaviour
{
    public int targetCount = 1;
    public const float delay = 1f;
    private List<EnforcableMachine> collected = new List<EnforcableMachine>();
    public Animator anim;

    // Start is called before the first frame update

    private void OnTriggerEnter(Collider other)
    {
        EnforcableMachine em = other.GetComponentInParent<EnforcableMachine>();
        if (em && !collected.Contains(em))
        {
            em.rgbd.velocity = em.rgbd.velocity / 5;
            collected.Add(em);
            if (anim) anim.SetTrigger("go");
            if (collected.Count >= targetCount)
            {
                FishSpace.Centralizer.Add_DelayedMonoAct(this,TargetReached,delay);
            }
        }
    }

    void TargetReached()
    {
        if (MainGameManager.instance)
        {

            MainGameManager.instance.runningLevelManager.AddStar();
            MainGameManager.instance.runningLevelManager.OnComplete(true);
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel);
        }

    }
}
