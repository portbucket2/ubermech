﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineMech : MonoBehaviour
{
    public bool log;
    public float addedPower = 1;
    public Animator anim;
    private void OnCollisionEnter(Collision collision)
    {
        EnforcableMachine em = collision.collider.GetComponentInParent<EnforcableMachine>();

        if (em != null)
        {
            if (log) Debug.LogFormat("point: {0}_____ vel: {1}",collision.contacts[0].point, collision.relativeVelocity);
            float v1 = collision.relativeVelocity.y;
            float h1 = Mathf.Abs(v1 * v1 / (2*Physics.gravity.y));

            float h2 = h1 + addedPower + em.personalTrampolineBonusPower;

            float v2 = Mathf.Sqrt(2*Mathf.Abs(Physics.gravity.y)*h2);

            em.rgbd.velocity = new Vector3(collision.relativeVelocity.x, v2, collision.relativeVelocity.z);

            anim.SetTrigger("go");
        }
    }
}
